diff --git a/Setup.hs b/Setup.hs
deleted file mode 100644
index f5e64fd..0000000
--- a/Setup.hs
+++ /dev/null
@@ -1,76 +0,0 @@
-module Main
-  ( main
-  ) where
-
-import           Data.List ( nub, sortOn )
-import           Distribution.InstalledPackageInfo
-                   ( sourcePackageId, installedUnitId )
-import           Distribution.Package ( UnitId, packageVersion, packageName )
-import           Distribution.PackageDescription
-                   ( PackageDescription (), Executable (..) )
-import           Distribution.Pretty ( prettyShow )
-import           Distribution.Simple
-                   ( defaultMainWithHooks, UserHooks(..), simpleUserHooks )
-import           Distribution.Simple.BuildPaths ( autogenPackageModulesDir )
-import           Distribution.Simple.LocalBuildInfo
-                   ( installedPkgs, withLibLBI, withExeLBI, LocalBuildInfo ()
-                   , ComponentLocalBuildInfo (componentPackageDeps)
-                   )
-import           Distribution.Simple.PackageIndex
-                   ( allPackages, dependencyClosure )
-import           Distribution.Simple.Setup
-                   ( BuildFlags (..), ReplFlags (..), fromFlag )
-import           Distribution.Simple.Utils
-                   ( rewriteFileEx, createDirectoryIfMissingVerbose )
-import           Distribution.Types.PackageName ( unPackageName )
-import           Distribution.Types.UnqualComponentName
-                   ( unUnqualComponentName )
-import           Distribution.Verbosity ( Verbosity, normal )
-import           System.FilePath ( (</>) )
-
-main :: IO ()
-main = defaultMainWithHooks simpleUserHooks
-  { buildHook = \pkg lbi hooks flags -> do
-      generateBuildModule (fromFlag (buildVerbosity flags)) pkg lbi
-      buildHook simpleUserHooks pkg lbi hooks flags
-    -- The 'cabal repl' hook corresponds to the 'cabal build' hook and is added
-    -- because, with a Cabal-based cradle, Haskell Language Server makes use of
-    -- 'cabal repl'.
-  , replHook = \pkg lbi hooks flags args -> do
-      generateBuildModule (fromFlag (replVerbosity flags)) pkg lbi
-      replHook simpleUserHooks pkg lbi hooks flags args
-  }
-
-generateBuildModule :: Verbosity -> PackageDescription -> LocalBuildInfo -> IO ()
-generateBuildModule verbosity pkg lbi = do
-  let dir = autogenPackageModulesDir lbi
-  createDirectoryIfMissingVerbose verbosity True dir
-  withLibLBI pkg lbi $ \_ libcfg -> do
-    withExeLBI pkg lbi $ \exe clbi ->
-      rewriteFileEx normal (dir </> "Build_" ++ exeName' exe ++ ".hs") $ unlines
-        [ "module Build_" ++ exeName' exe
-        , "  ( deps"
-        , "  ) where"
-        , ""
-        , "deps :: [String]"
-        , "deps = " ++ show (formatdeps (transDeps libcfg clbi))
-        ]
-  where
-    exeName' = unUnqualComponentName . exeName
-    formatdeps = map formatone . sortOn unPackageName'
-    formatone p = unPackageName' p ++ "-" ++ prettyShow (packageVersion p)
-    unPackageName' = unPackageName . packageName
-    transDeps xs ys =
-      either (map sourcePackageId . allPackages) handleDepClosureFailure $ dependencyClosure allInstPkgsIdx availInstPkgIds
-      where
-        allInstPkgsIdx = installedPkgs lbi
-        allInstPkgIds = map installedUnitId $ allPackages allInstPkgsIdx
-        -- instPkgIds includes `stack-X.X.X`, which is not a dependency hence is missing from allInstPkgsIdx. Filter that out.
-        availInstPkgIds = filter (`elem` allInstPkgIds) $ testDeps xs ys
-        handleDepClosureFailure unsatisfied =
-          error $
-            "Computation of transitive dependencies failed." ++
-            if null unsatisfied then "" else " Unresolved dependencies: " ++ show unsatisfied
-
-testDeps :: ComponentLocalBuildInfo -> ComponentLocalBuildInfo -> [UnitId]
-testDeps xs ys = map fst $ nub $ componentPackageDeps xs ++ componentPackageDeps ys
diff --git a/SetupHooks.hs b/SetupHooks.hs
new file mode 100644
index 0000000..8ca1acf
--- /dev/null
+++ b/SetupHooks.hs
@@ -0,0 +1,119 @@
+{-# LANGUAGE OverloadedStrings #-}
+{-# LANGUAGE OverloadedLists #-}
+{-# LANGUAGE DisambiguateRecordFields #-}
+{-# LANGUAGE StaticPointers #-}
+
+{-# OPTIONS_GHC -fno-warn-warnings-deprecations #-}
+
+module SetupHooks
+  ( setupHooks
+  ) where
+
+import           Data.List ( nub, sortBy )
+import           Data.Ord ( comparing )
+import           Distribution.InstalledPackageInfo
+                   ( sourcePackageId, installedUnitId )
+import           Distribution.Package
+                   ( PackageId, UnitId, packageVersion, packageName )
+import           Distribution.PackageDescription
+                   ( PackageDescription (package), Executable (..)
+                   , componentNameRaw )
+import           Distribution.Pretty ( prettyShow )
+import           Distribution.Simple
+                   ( defaultMainWithHooks, UserHooks(..), simpleUserHooks )
+import           Distribution.Simple.BuildPaths ( autogenComponentModulesDir )
+import           Distribution.Simple.LocalBuildInfo
+import           Distribution.Simple.PackageIndex
+                   ( allPackages, dependencyClosure )
+import           Distribution.Simple.Setup
+                   ( BuildFlags (buildVerbosity), fromFlag )
+import           Distribution.Simple.SetupHooks
+import           Distribution.Simple.Utils
+                   ( rewriteFileEx, createDirectoryIfMissingVerbose )
+import           Distribution.Types.PackageName ( PackageName, unPackageName )
+import           Distribution.Types.UnqualComponentName
+                   ( unUnqualComponentName )
+import           Distribution.Verbosity ( Verbosity, normal )
+import           System.FilePath ( takeDirectory, (</>) )
+import           Distribution.Utils.Path ( getSymbolicPath, makeRelativePathEx )
+
+setupHooks :: SetupHooks
+setupHooks =
+  noSetupHooks
+    { -- autogen-modules already registers Build_...
+      buildHooks =
+       noBuildHooks
+         { preBuildComponentRules = Just $ (rules (static ()) preBuildHook) }
+    }
+
+preBuildHook :: PreBuildComponentInputs -> RulesM ()
+preBuildHook
+  pbci@PreBuildComponentInputs
+    { buildingWhat=flags
+    , localBuildInfo=lbi
+    , targetInfo=tgt
+    }
+  | CLibName LMainLibName <- componentName $ targetComponent tgt
+  = do
+    let result = buildModuleResult lbi tgt (localPkgDescr lbi)
+    let genbuild = mkCommand (static Dict) (static generateBuildModule) pbci
+    registerRule_ "Build_*" $ staticRule genbuild [] [result]
+  | otherwise
+  = return ()
+
+generateBuildModule :: PreBuildComponentInputs -> IO ()
+generateBuildModule 
+  PreBuildComponentInputs
+    { buildingWhat=flags
+    , localBuildInfo=lbi
+    , targetInfo=mainLibTargetInfo
+    }
+ = do
+
+   -- Generate a module in the stack library component that lists all the
+   -- dependencies of stack (both the library and the executable).
+   createDirectoryIfMissingVerbose verbosity True (takeDirectory buildModulePath)
+   withExeLBI pkg lbi $ \ _ exeCLBI -> do
+     rewriteFileEx normal buildModulePath $ unlines
+       [ "module Build_" ++ pkgNm
+       , "  ( deps"
+       , "  ) where"
+       , ""
+       , "deps :: [String]"
+       , "deps = " ++ (show $ formatdeps (transDeps mainLibCLBI exeCLBI))
+       ]
+
+  where
+    mainLibCLBI = targetCLBI mainLibTargetInfo
+    formatdeps = map formatone . sortBy (comparing unPackageName')
+    formatone p = unPackageName' p ++ "-" ++ prettyShow (packageVersion p)
+    unPackageName' = unPackageName . packageName
+    transDeps xs ys =
+      either (map sourcePackageId . allPackages) handleDepClosureFailure $ dependencyClosure allInstPkgsIdx availInstPkgIds
+      where
+        allInstPkgsIdx = installedPkgs lbi
+        allInstPkgIds = map installedUnitId $ allPackages allInstPkgsIdx
+        -- instPkgIds includes `stack-X.X.X`, which is not a dependency hence is missing from allInstPkgsIdx. Filter that out.
+        availInstPkgIds = filter (`elem` allInstPkgIds) $ testDeps xs ys
+        handleDepClosureFailure unsatisfied =
+          error $
+            "Computation of transitive dependencies failed." ++
+            if null unsatisfied then "" else " Unresolved dependencies: " ++ show unsatisfied
+
+    pkgNm :: String
+    pkgNm = unPackageName' $ package pkg
+
+    pkg = localPkgDescr lbi
+    verbosity = buildingWhatVerbosity flags
+    buildModulePath = getSymbolicPath $ location (buildModuleResult lbi mainLibTargetInfo pkg)
+
+buildModuleResult :: LocalBuildInfo -> TargetInfo -> PackageDescription -> Location
+buildModuleResult lbi tgt pkg = Location autogendir (makeRelativePathEx $ "Build_" ++ pkgNm ++ ".hs")
+  where
+    unPackageName' = unPackageName . packageName
+    pkgNm = unPackageName' $ package pkg
+    -- We want the symbolic path relative to the package.
+    autogendir = autogenComponentModulesDir lbi (targetCLBI tgt)
+
+testDeps :: ComponentLocalBuildInfo -> ComponentLocalBuildInfo -> [UnitId]
+testDeps xs ys = map fst $ nub $ componentPackageDeps xs ++ componentPackageDeps ys
diff --git a/src/Stack/ComponentFile.hs b/src/Stack/ComponentFile.hs
index 774de84..0ac57c4 100644
--- a/src/Stack/ComponentFile.hs
+++ b/src/Stack/ComponentFile.hs
@@ -74,7 +74,7 @@ benchmarkFiles component bench =
   names = bnames <> exposed
   exposed =
     case benchmarkInterface bench of
-      BenchmarkExeV10 _ fp -> [DotCabalMain fp]
+      BenchmarkExeV10 _ fp -> [DotCabalMain (getSymbolicPath fp)]
       BenchmarkUnsupported _ -> []
   bnames = map DotCabalModule (otherModules build)
   build = benchmarkBuildInfo bench
@@ -92,7 +92,7 @@ testFiles component test =
   names = bnames <> exposed
   exposed =
     case testInterface test of
-      TestSuiteExeV10 _ fp -> [DotCabalMain fp]
+      TestSuiteExeV10 _ fp -> [DotCabalMain (getSymbolicPath fp)]
       TestSuiteLibV09 _ mn -> [DotCabalModule mn]
       TestSuiteUnsupported _ -> []
   bnames = map DotCabalModule (otherModules build)
@@ -111,7 +111,7 @@ executableFiles component exe =
   build = buildInfo exe
   names =
     map DotCabalModule (otherModules build) ++
-    [DotCabalMain (modulePath exe)]
+    [DotCabalMain (getSymbolicPath $ modulePath exe)]
 
 -- | Get all files referenced by the library.
 libraryFiles ::
@@ -459,13 +459,13 @@ buildOtherSources build = do
               warnMissingFile "File" cwd fp file
               pure Nothing
             Just p -> pure $ Just (toCabalPath p)
-  csources <- resolveDirFiles (cSources build) DotCabalCFilePath
+  csources <- resolveDirFiles (map getSymbolicPath $ cSources build) DotCabalCFilePath
   jsources <- resolveDirFiles (targetJsSources build) DotCabalFilePath
   pure (csources <> jsources)
 
 -- | Get the target's JS sources.
 targetJsSources :: BuildInfo -> [FilePath]
-targetJsSources = jsSources
+targetJsSources = map getSymbolicPath . jsSources
 
 -- | Resolve file as a child of a specified directory, symlinks
 -- don't get followed.
diff --git a/src/Stack/Package.hs b/src/Stack/Package.hs
index 629973d..878e72d 100644
--- a/src/Stack/Package.hs
+++ b/src/Stack/Package.hs
@@ -84,6 +84,7 @@ import           System.FilePath ( replaceExtension )
 import           Stack.Types.Dependency ( DepValue (..), DepType (..) )
 import           Stack.Types.PackageFile ( DotCabalPath , GetPackageFiles (..) )
 import           Stack.PackageFile ( getPackageFile )
+import           Distribution.Utils.Path ( makeSymbolicPath )
 
 -- | Read @<package>.buildinfo@ ancillary files produced by some Setup.hs hooks.
 -- The file includes Cabal file syntax to be merged into the package description
@@ -92,7 +93,7 @@ import           Stack.PackageFile ( getPackageFile )
 -- NOTE: not to be confused with BuildInfo, an Stack-internal datatype.
 readDotBuildinfo :: MonadIO m => Path Abs File -> m HookedBuildInfo
 readDotBuildinfo buildinfofp =
-  liftIO $ readHookedBuildInfo silent (toFilePath buildinfofp)
+  liftIO $ readHookedBuildInfo silent Nothing (makeSymbolicPath $ toFilePath buildinfofp)
 
 -- | Resolve a parsed Cabal file into a 'Package', which contains all of the
 -- info needed for Stack to build the 'Package' given the current configuration.
@@ -405,7 +406,7 @@ generateBuildInfoOpts BioInput {..} =
   pkgIncludeOpts =
     [ toFilePathNoTrailingSep absDir
     | dir <- includeDirs biBuildInfo
-    , absDir <- handleDir dir
+    , absDir <- handleDir (getSymbolicPath dir)
     ]
   libOpts =
     map ("-l" <>) (extraLibs biBuildInfo) <>
@@ -413,13 +414,13 @@ generateBuildInfoOpts BioInput {..} =
   pkgLibDirs =
     [ toFilePathNoTrailingSep absDir
     | dir <- extraLibDirs biBuildInfo
-    , absDir <- handleDir dir
+    , absDir <- handleDir (getSymbolicPath dir)
     ]
   handleDir dir = case (parseAbsDir dir, parseRelDir dir) of
     (Just ab, _       ) -> [ab]
     (_      , Just rel) -> [biCabalDir </> rel]
     (Nothing, Nothing ) -> []
-  fworks = map ("-framework=" <>) (frameworks biBuildInfo)
+  fworks = map (("-framework=" <>) . getSymbolicPath) (frameworks biBuildInfo)
 
 -- | Make the .o path from the .c file path for a component. Example:
 --
diff --git a/src/Stack/PackageFile.hs b/src/Stack/PackageFile.hs
index 1978e6b..eb65194 100644
--- a/src/Stack/PackageFile.hs
+++ b/src/Stack/PackageFile.hs
@@ -42,6 +42,7 @@ import           Stack.Types.PackageFile
                    )
 import qualified System.FilePath as FilePath
 import           System.IO.Error ( isUserError )
+import           Distribution.Utils.Path (getSymbolicPath, makeSymbolicPath)
 
 -- | Resolve the file, if it can't be resolved, warn for the user
 -- (purely to be helpful).
@@ -94,8 +95,9 @@ packageDescModulesAndFiles pkg = do
       )
   dfiles <- resolveGlobFiles
               (specVersion pkg)
-              ( extraSrcFiles pkg
-                ++ map (dataDir pkg FilePath.</>) (dataFiles pkg)
+              ( map getSymbolicPath (extraSrcFiles pkg)
+                ++ map (getSymbolicPath (dataDir pkg) FilePath.</>)
+                    (map getSymbolicPath (dataFiles pkg))
               )
   let modules = libraryMods <> subLibrariesMods <> executableMods <> testMods <>
                   benchModules
@@ -134,10 +136,10 @@ resolveGlobFiles cabalFileVersion =
   explode name = do
     dir <- asks (parent . ctxFile)
     names <- matchDirFileGlob' (toFilePath dir) name
-    mapM resolveFileOrWarn names
+    mapM (resolveFileOrWarn . getSymbolicPath) names
   matchDirFileGlob' dir glob =
     catch
-      (liftIO (matchDirFileGlob minBound cabalFileVersion dir glob))
+      (liftIO (matchDirFileGlob minBound cabalFileVersion (Just $ makeSymbolicPath dir) (makeSymbolicPath glob)))
       ( \(e :: IOException) ->
         if isUserError e
           then do
diff --git a/src/Stack/SDist.hs b/src/Stack/SDist.hs
index e4b8473..6f23a7d 100644
--- a/src/Stack/SDist.hs
+++ b/src/Stack/SDist.hs
@@ -591,8 +591,8 @@ checkPackageInExtractedTarball pkgDir = do
         -- whereas flattenPackageDescription from Cabal does. In any event,
         -- using `Nothing` seems more logical for this check anyway, and the
         -- fallback to `Just pkgDesc` is just a crazy sanity check.
-        case Check.checkPackage gpd Nothing of
-          [] -> Check.checkPackage gpd (Just pkgDesc)
+        case Check.checkPackage gpd of
+          [] -> Check.checkPackage gpd -- (Just pkgDesc)
           x -> x
   fileChecks <-
     liftIO $ Check.checkPackageFiles minBound pkgDesc (toFilePath pkgDir)
diff --git a/stack.cabal b/stack.cabal
index fecb7c5..9d464a6 100644
--- a/stack.cabal
+++ b/stack.cabal
@@ -1,7 +1,7 @@
-cabal-version:      2.0
+cabal-version:      3.14
 name:               stack
 version:            2.13.1
-license:            BSD3
+license:            BSD-3-Clause
 license-file:       LICENSE
 maintainer:         manny@fpcomplete.com
 author:             Commercial Haskell SIG
@@ -23,7 +23,7 @@ description:
     currently only intended for use by the executable.
 
 category:           Development
-build-type:         Custom
+build-type:         Hooks
 extra-source-files:
     CONTRIBUTING.md
     ChangeLog.md
@@ -107,7 +107,8 @@ source-repository head
 
 custom-setup
     setup-depends:
-        Cabal >=3.8.1.0 && <3.12,
+        Cabal >=3.8.1.0 && <3.15,
+        Cabal-hooks,
         base >=4.14.3.0 && <5,
         filepath >=1.4.2.2
 
@@ -382,7 +383,7 @@ library
         neat-interpolation >=0.5.1.3,
         open-browser >=0.2.1.0,
         optparse-applicative >=0.18.1.0,
-        pantry >=0.9.2,
+        pantry ==0.9.2,
         path >=0.9.2,
         path-io >=1.8.1,
         persistent >=2.14.0.0 && <2.15,
@@ -503,7 +504,7 @@ executable stack
         neat-interpolation >=0.5.1.3,
         open-browser >=0.2.1.0,
         optparse-applicative >=0.18.1.0,
-        pantry >=0.9.2,
+        pantry ==0.9.2,
         path >=0.9.2,
         path-io >=1.8.1,
         persistent >=2.14.0.0 && <2.15,
