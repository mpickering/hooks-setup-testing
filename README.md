# Overlay Hackage Package Index for Hooks build type

This repository contains packages patched to use the `Hooks` build-type.
It can be used as an overlay much like `head.hackage`.

## Usage information

To allow `cabal-install` to pull patches from this repository, you should
add the following to your `cabal.project` or `cabal.project.local` file:

```
repository hooks-setup-testing.ghc.haskell.org
   url: https://mpickering.gitlab.haskell.org/hooks-setup-testing/
   secure: True
   key-threshold: 3
   root-keys:
       46b3ab3433bf273303a01b658a9be7f166e525c07f1ad6598e9d1d7c4ea0bf46
       f2cd708c5a68b805f0e150d75811c16eb1fc237be52a5584fb5b3b8e1e990ac9
       80bda11a92009dcf3ff70696475f1564b55b8d6796e0588929193a45022b8509
       29b00e90e759b822be58c3dc43c77fa4aba0b0fa6a06f611e26ba77f00b77feb
       4e083c2c26f3a9231560eb8b2c0a88ab6b661039fce3165cd2936514d771fb9c
       b20a85388d60c2d74ad52240dc23e7dea3bb21bfb1b9ffdfbe6fe3a771ca168b

active-repositories: hackage.haskell.org, hooks-setup-testing.ghc.haskell.org:override
```

The use of `:override` forces `cabal`'s constraint solver to pick versions of
libraries that have corresponding patches in this overlay repository whenever
possible.

## Development information

### Updating Cabal commit

To update the commit of cabal we point to run:

```
nix flake lock --update-input mpickering-cabal
```

