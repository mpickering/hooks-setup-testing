{-# LANGUAGE OverloadedStrings #-}

module MakeConstraints where

import qualified Distribution.Package as Cabal
import Distribution.Text
import Distribution.Types.Version hiding (showVersion)

import qualified Data.Set as S
import qualified Data.Map.Strict as M

import qualified Text.PrettyPrint.ANSI.Leijen as PP
import Text.PrettyPrint.ANSI.Leijen (Doc, vcat, (<+>))

import Utils

extraConstraints :: [String]
extraConstraints = [
    "setup.Cabal == 3.13.0.0"
  , "Cabal == 3.13.0.0"
  ]

-- These packages we must use the installed version, because there's no way to upgrade
-- them
bootPkgs :: S.Set Cabal.PackageName
bootPkgs = S.fromList
  [ "base"
  , "template-haskell"
  , "ghc"
  , "ghc-prim"
  , "integer-gmp"
  , "ghc-bignum"
  ]

-- These packages are installed, but we can install newer versions if the build plan
-- allows.. so we --allow-newer them in order to help find more build plans.
allowNewerPkgs :: S.Set Cabal.PackageName
allowNewerPkgs = S.fromList
  [ "time"
  , "binary"
  , "bytestring"
  , "Cabal"
  , "deepseq"
  , "text" ] `S.union` bootPkgs

constraints :: [String] -> Doc
constraints constraints =
  "constraints:" PP.<$$> PP.indent 2 constraintsDoc
  where
    constraintsDoc = PP.vcat $ PP.punctuate "," (map PP.text constraints)

allowNewer :: S.Set Cabal.PackageName -> Doc
allowNewer pkgs =
  "allow-newer:" PP.<$$> PP.indent 2 pkgsDoc
  where
    pkgsDoc = PP.vcat $ PP.punctuate "," $ map prettyPackageName $ S.toList pkgs

installedConstraints :: S.Set Cabal.PackageName -> S.Set Cabal.PackageName -> Doc
installedConstraints bootPkgs patchedPkgs =
  "constraints:" PP.<$$> PP.indent 2 pkgsDoc
  where
    pkgsDoc = PP.vcat $ PP.punctuate ","
      [ prettyPackageName bootPkg <+> "installed"
      | bootPkg <- S.toList bootPkgs
      , bootPkg `S.notMember` patchedPkgs
      ]

versionConstraints :: [(Cabal.PackageName, Version)] -> Doc
versionConstraints pkgs =
  "constraints:" PP.<$$> PP.indent 2 body
  where
    body :: Doc
    body = vcat $ PP.punctuate ","
     [ prettyPackageName pkg <+> versionConstraints vers
     | (pkg, vers) <- M.toList pkgVersions
     ]

    versionConstraints :: S.Set Version -> Doc
    versionConstraints vers =
      PP.hcat $ PP.punctuate " || "
      [ "==" <> prettyVersion ver
      | ver <- S.toAscList vers
      ]

    pkgVersions :: M.Map Cabal.PackageName (S.Set Version)
    pkgVersions = M.fromListWith (<>)
      [ (pkg, S.singleton ver)
      | (pkg, ver) <- pkgs
      ]

makeConstraints :: FilePath -- ^ patch directory
                -> IO Doc
makeConstraints patchDir = do
  patches <- findPatchedPackages patchDir
  let patchedPkgs = S.fromList $ map fst patches
      doc = PP.vcat
        [ allowNewer allowNewerPkgs
        , ""
        , installedConstraints bootPkgs patchedPkgs
        , ""
        , versionConstraints patches
        , ""
        , constraints extraConstraints
        ]
  return doc
