# vi: set filetype=sh

# Packages expected not to build due to GHC bugs. This is `source`'d by the CI
# script and the arguments in BROKEN_ARGS are added to the hackage-ci
# command-line.

# Mark the named package as broken.
#
# Usage:
#    broken $pkg_name $ghc_ticket_number
#
function broken() {
  pkg_name="$1"
  ticket="$2"
  echo "Marking $pkg_name as broken due to #$ticket"
  EXTRA_OPTS="$EXTRA_OPTS --expect-broken=$pkg_name"
}

function only_package() {
  echo "Adding $@ to --only package list"
  for pkg in $@; do
    EXTRA_OPTS="$EXTRA_OPTS --only=$pkg"
  done
}

function test_package() {
  echo "Adding $@ to --test-package list"
  EXTRA_OPTS="$EXTRA_OPTS --test-package=$1=$2"
}

# Return the version number of the most recent release of the given package
function latest_version() {
  pkg=$1
  curl -s -H "Accept: application/json" -L -X GET http://hackage.haskell.org/package/$pkg/preferred | jq '.["normal-version"] | .[0]' -r
}

# Add a package to the set of packages that lack patches but are nevertheless
# tested.
function extra_package() {
  pkg_name="$1"
  version="$2"
  if [ -z "$version" ]; then
    version=$(latest_version $pkg_name)
  fi
  echo "Adding $pkg_name-$version to extra package set"
  EXTRA_OPTS="$EXTRA_OPTS --extra-package=$pkg_name==$version"
}

# Mark a package to be declared with build-tool-depends, not build-depends.
# This is necessary for packages that do not have a library component.
function build_tool_package() {
  pkg_name="$1"
  echo "Adding $pkg_name as a build-tool package"
  EXTRA_OPTS="$EXTRA_OPTS --build-tool-package=$pkg_name"
}

if [ -z "$GHC" ]; then GHC=ghc; fi

function ghc_version() {
  $GHC --version | sed 's/.*version \([0-9]*\.\([0-9]*\.\)*\)/\1/'
}

function ghc_commit() {
  $GHC --print-project-git-commit-id
}

function ghc_arch() {
  $GHC --print-host-platform
}

# ======================================================================
# Baseline constraints
#
# These constraints are applied to preclude the solver from producing build
# plans using ancient, under-constrained package versions.

EXTRA_OPTS="$EXTRA_OPTS --extra-cabal-fragment=$(pwd)/config.cabal.project"

# ======================================================================
# The lists begin here
#
# For instance:
#
#    broken "lens" 17988

version="$(ghc_version)"
commit="$(ghc_commit)"
arch="$(ghc_arch)"
echo "Found GHC $version, commit $commit."
case $version in
  9.4.*)
    #       package                   ticket
    broken  linear-generics           22546
    ;;

  9.6.*)
    #       package                   ticket
    ;;

  9.8.*)
    #       package                   ticket
    ;;

  9.9.*)
    #       package                   ticket
    ;;

  *)
    echo "No broken packages for GHC $version"
    ;;
esac

case $arch in
  x86_64-*-*)
  #       package                   ticket
  ;;
  aarch64-*-*)
  # These just don't build on aarch64
  #       package                   ticket
  broken  charsetdetect             00000
  broken  packman                   00000
  ;;
  *)
  echo "$arch is unknown to head.hackage, assuming nothing is broken."
  ;;
esac



