{ sources ? import ./nix/sources.nix, nixpkgs ? (import sources.nixpkgs.outPath {}) }:


with nixpkgs;
let
  haskellPackages_ = nixpkgs.haskell.packages.ghc963.override {
       all-cabal-hashes = sources.all-cabal-hashes.outPath;
       overrides = self: super: {
         mkDerivation = args: super.mkDerivation (args // {
           enableLibraryProfiling = false;
           doCheck = false;
           doHoogle = false;
           doHaddock = false;
         });
       };
     };

  cabalSrc = sources.mpickering-cabal.outPath;

  haskellPackagesWithCabal = haskellPackages.extend (self: super:
        cabalDependenciesOverrides self
  );

  haskellPackages = haskellPackages_.extend (self: super:
        {
        semaphore-compat = self.callHackage "semaphore-compat" "1.0.0" {};
        cabal-plan = self.callHackage "cabal-plan" "0.7.3.0" {};
        hackage-security = self.callHackage "hackage-security" "0.6.2.6" {};

  });


  hackage-repo-tool =
    let src = sources.hackage-security.outPath;
    in nixpkgs.haskell.lib.doJailbreak (haskellPackages.callCabal2nix "hackage-repo-tool" "${src}/hackage-repo-tool" {});

  overlay-tool =
    let src = sources.overlay-tool;
    in nixpkgs.haskell.lib.doJailbreak (haskellPackages.callCabal2nix "hackage-overlay-repo-tool" src { });

  head-hackage-ci =
    let
      src = nixpkgs.nix-gitignore.gitignoreSource [] ./.;
    in haskellPackages.callCabal2nix "head-hackage-ci" src {};

  buildDeps = import ./build-deps.nix { pkgs = nixpkgs; };

  cabalDependencies = [ "Cabal-tests" "Cabal" "cabal-install-solver" "Cabal-syntax" "Cabal-hooks" "Cabal-described" "Cabal-QuickCheck" "Cabal-tree-diff" "cabal-install" ];

  cabalDependenciesOverrides = self:
    builtins.listToAttrs (builtins.map (pn: { name = "${pn}"; value = self.callCabal2nixWithOptions pn "${cabalSrc}/${pn}" "--no-check" {};} ) cabalDependencies);

  commonCabalFragment = ''
    active-repositories:
      hackage.haskell.org
      , local
      , my-local-repository:override

    constraints:
      setup.Cabal >= 3.13.0.0
      , Cabal >= 3.13.0.0


    repository my-local-repository
      url: file+noindex://${tar_dirs}#shared-cache
  '';

  tar_dir = pn:
    let v = if pn == "Cabal-hooks" then "0.1" else "3.13.0.0";
    in runCommand "tar" { nativeBuildInputs = []; }
  '' mkdir -p $out
     cp -r ${cabalSrc}/${pn} ${pn}-${v}
     tar -czvf $out/${pn}-${v}.tar.gz ${pn}-${v}'';

  tar_dirs = symlinkJoin { name = "tar_dirs"; paths = map (pn: tar_dir pn) cabalDependencies; };



  buildDepsFragment =
    let

      mkCabalFragment = pkgName: deps:
        with pkgs.lib;
        let
          libDirs = concatStringsSep " " (map (dep: getOutput "lib" dep + "/lib") deps);
          includeDirs = concatStringsSep " " (map (dep: getOutput "dev" dep + "/include") deps);
        in ''
        package ${pkgName}
          extra-lib-dirs: ${libDirs}
          extra-include-dirs: ${includeDirs}
        '';
    in

      pkgs.lib.concatStringsSep "\n"
        ([ commonCabalFragment ] ++ (pkgs.lib.mapAttrsToList mkCabalFragment buildDeps));

  buildDepsFile = pkgs.writeText "deps.cabal.project" buildDepsFragment;

  build-repo =
    let
      deps = [
        bash curl gnutar findutils patch rsync openssl
        haskellPackagesWithCabal.cabal-install haskellPackages.ghc gcc binutils-unwrapped pwgen gnused
        hackage-repo-tool overlay-tool python3 jq pkg-config
        git # cabal-install wants this to fetch source-repository-packages
        postgresql libmysqlclient.dev
      ];

      pkg_config_depends = lib.makeSearchPathOutput "dev" "lib/pkgconfig" (lib.concatLists (lib.attrValues buildDeps));

    in
      runCommand "repo" {
        nativeBuildInputs = [ makeWrapper ];
      } ''
        mkdir -p $out/bin
        makeWrapper ${head-hackage-ci}/bin/head-hackage-ci $out/bin/head-hackage-ci \
            --prefix PATH : ${lib.makeBinPath deps}:$out/bin

        makeWrapper ${./build-repo.sh} $out/bin/build-repo.sh \
            --prefix PATH : ${lib.makeBinPath deps}:$out/bin \
            --set CABAL_PKGS_DIR ${tar_dirs}

        makeWrapper ${./discover_tarball.sh} $out/bin/discover_tarball.sh \
            --prefix PATH : ${lib.makeBinPath deps}:$out/bin

        makeWrapper ${../run-ci} $out/bin/run-ci \
            --prefix PATH : ${lib.makeBinPath deps}:$out/bin \
            --prefix PKG_CONFIG_PATH : ${pkg_config_depends} \
            --set HASKELL_GI_GIR_SEARCH_PATH ${gobject-introspection.dev}/share/gir-1.0 \
            --set USE_NIX 1 \
            --set CI_CONFIG ${./config.sh}

        makeWrapper ${./find-job.sh} $out/bin/find-job \
            --prefix PATH : ${lib.makeBinPath deps}:$out/bin

        makeWrapper ${./find-latest-job.sh} $out/bin/find-latest-job \
            --prefix PATH : ${lib.makeBinPath deps}:$out/bin

        makeWrapper ${xz}/bin/xz $out/bin/xz
        makeWrapper ${curl}/bin/curl $out/bin/curl
      '';
in
  mkShell {
    name = "head-hackage-build-env";
    buildInputs = [ build-repo haskellPackagesWithCabal.cabal-install haskellPackages.ghc ];
    cabalDepsSrc = buildDepsFragment;
  }
