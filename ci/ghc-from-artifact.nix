let
  sources = import ./nix/sources.nix;
  nixpkgs = import sources.nixpkgs {};
in

{ ghcTarball }:
let
  nixpkgsSrc = sources.nixpkgs;
  ghc-artefact-nix =
    import sources.ghc-artefact-nix {
      url = ghcTarball;
      nixpkgsSrc = sources.nixpkgs;
    };
in ghc-artefact-nix.ghcHEAD
